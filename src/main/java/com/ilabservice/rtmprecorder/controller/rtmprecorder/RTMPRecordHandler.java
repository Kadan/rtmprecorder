package com.ilabservice.rtmprecorder.controller.rtmprecorder;
import com.ilabservice.rtmprecorder.constants.Constants;
import com.ilabservice.rtmprecorder.entity.Video;
import com.ilabservice.rtmprecorder.ffmpeg.FFMPEGUtils;
import com.ilabservice.rtmprecorder.service.VideoService;
import com.ilabservice.rtmprecorder.util.Handler;
import com.ilabservice.rtmprecorder.util.RecorderRedisHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class RTMPRecordHandler {

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    private static final String RTMP_SUFIX = "-rtmp";

    @Autowired
    FFMPEGUtils ffmpegUtils;

    @Autowired
    Handler handler;

    @Autowired
    VideoService videoService;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;


    @Value("${rtmpRecordSaveMax}")
    private int rtmpRecordSaveMax;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    private static final Logger log = LoggerFactory.getLogger(RTMPRecordHandler.class);

    @PostMapping("/save/recording/camera/{sn}/start/{timestamp}/length/{len}")
    public ResponseEntity saveRTMPRecord(HttpServletRequest request, @PathVariable String sn
            , @PathVariable long timestamp,@PathVariable int len) {

        Map<String,String> rtmpRecorderMap = recorderRedisHandler.getRTMPRecorderMap();

        if(!rtmpRecorderMap.keySet().contains(sn+RTMP_SUFIX))
            return new ResponseEntity("The stream " + sn + " is not being recorded .", HttpStatus.NOT_FOUND);

        if(rtmpRecordSaveMax<=0)
            rtmpRecordSaveMax=30;
        if(len > rtmpRecordSaveMax*60)
            return new ResponseEntity("The maximum record length can be saved is " + rtmpRecordSaveMax + " minutes", HttpStatus.BAD_REQUEST);

        if(timestamp >= System.currentTimeMillis())
            return new ResponseEntity("Requested start time should be earlier than now()", HttpStatus.BAD_REQUEST);

        String rtmpUrl = rtmpRecorderMap.get(sn+RTMP_SUFIX);

        if(!videoRecordPath_temp.endsWith(File.separator)) videoRecordPath_temp = videoRecordPath_temp + File.separator;
        //probe into the temp path for the sn
        String recordPath = videoRecordPath_temp +
                sn + RTMP_SUFIX + File.separator;
        if(!videoRecordPath.endsWith(File.separator)) videoRecordPath = videoRecordPath + File.separator;
        if(!fileDownloadPath.endsWith(File.separator))fileDownloadPath=fileDownloadPath+ File.separator;

        String savePath = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L);
        String outputFile = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator + timestamp + "-" + (timestamp+len*1000L) + ".mp4";
        String outputImg = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator + timestamp + "-" + (timestamp+len*1000L) + ".jpg";

        if(new File(outputFile).exists() && new File(outputImg).exists())
            return new ResponseEntity(" Record already exists for the stream " + sn + " with the start timestamp " + timestamp + " and given length " + len, HttpStatus.BAD_REQUEST);

        if(new File(recordPath).exists()) {
            File[] rawFiles = new File(recordPath).listFiles();
            if (rawFiles != null && rawFiles.length > 0) {

                List<File> readyFiles = Arrays.stream(rawFiles).filter(f -> StringUtils.containsIgnoreCase(f.getName(), "raw-ready")
                        && StringUtils.containsIgnoreCase(f.getName(), ".mp4")).collect(Collectors.toList());
                //sort the files
                Collections.sort(readyFiles);
                //the first file's creation time
                long firstCreated = Long.valueOf(StringUtils.substringBetween(readyFiles.get(0).getName(), "raw-ready", ".mp4"));
                if (timestamp < firstCreated && (timestamp + len * 1000L) < firstCreated) {
                    return new ResponseEntity("No record found for given time frame (start time) " + timestamp + " and Length " + len + " for the stream " + sn + " is not being recorded .", HttpStatus.NOT_FOUND);
                }
                new Thread(() -> {
                    int startOffSet = 0;
                    int duration = 0;
                    int totalDuration = 0;
                    recorderRedisHandler.saveStoppingStatus(sn + RTMP_SUFIX);
                    //check the required end time
                    long requiredEnd = timestamp + len * 1000L;
                    long lastFileStart = Long.valueOf(StringUtils.substringBetween(readyFiles.get(readyFiles.size() - 1).getName(), "raw-ready", ".mp4"));
                    if (requiredEnd >= lastFileStart) {
                        //break the recording for now
                        handler.stopRTMPRecord(rtmpUrl);

                    } else {
                        //ignore the in recording file
                        if (readyFiles.size() > 1)
                            readyFiles.remove(readyFiles.size() - 1);
                        else
                            handler.stopRTMPRecord(rtmpUrl);
                    }

                    //delete the previous index file
                    if (new File(recordPath + "index.txt").exists())
                        new File(recordPath + "index.txt").delete();
                    try {
                        PrintWriter out = new PrintWriter(new BufferedWriter(
                                new FileWriter(recordPath + "index.txt", true)));

                        List<String> vaildList = new ArrayList<String>();

                        for (int i = 0; i < readyFiles.size(); i++) {
                            String fName = readyFiles.get(i).getName();
                            long created = Long.valueOf(StringUtils.substringBetween(fName,"raw-ready",".mp4"));
                            long lastModify = readyFiles.get(i).lastModified();
                            if (ffmpegUtils.validVideo(recordPath + fName) && isFileInScope(created,timestamp,lastModify)) {//only writes the valid files which is fbs > 50
                                out.println("file " + fName);
                                totalDuration = totalDuration + ffmpegUtils.getVideoTime(recordPath + fName);
                                vaildList.add(fName);
                                //getDuration(Long.valueOf(StringUtils.substringBetween(readyFiles.get(i).getName(),"raw-ready",".mp4")),readyFiles.get(i).lastModified());
                            } else
                                continue;
                        }

                        if (out != null)
                            out.close();

                        Collections.sort(vaildList);
                        if(vaildList.size()>0){
                            long firstCreatedValid = Long.valueOf(StringUtils.substringBetween(vaildList.get(0),"raw-ready",".mp4"));
                            if (timestamp > firstCreatedValid)
                                startOffSet = getDuration(firstCreatedValid, timestamp);
                        }

                        if (len > totalDuration)
                            duration = totalDuration;
                        else
                            duration = len;

                        if (!new File(savePath).exists()) new File(savePath).mkdirs();

                        ffmpegUtils.concatWithStartoffsetAndDuration(recordPath + "index.txt", outputFile, startOffSet, duration, outputImg);

                        recorderRedisHandler.removeStoppingStatus(sn + RTMP_SUFIX);

                        Video v = new Video();
                        v.setSize(new File(outputFile).length());
                        v.setCameraId(sn);
                        v.setVideo(StringUtils.replace(outputFile, videoRecordPath, fileDownloadPath));
                        v.setImage(StringUtils.replace(outputImg, videoRecordPath, fileDownloadPath));
                        v.setStartTime(timestamp);
                        v.setEndTime((timestamp + len * 1000L));
                        v.setStatus(Constants.CREATED);
                        v.setLength(Long.valueOf(duration));
                        videoService.save(v);

                    } catch (IOException e) {
                        log.info("could not prepare the index file " + recordPath + "index.txt");
                    }

                }).start();

                //ffmpegUtils.tailorVideo();

            } else
                new ResponseEntity("No recorded files found for the given start " +
                        timestamp + " and duratiojn " + len + " for the camera " + sn, HttpStatus.NOT_FOUND);
        }else{
            new ResponseEntity("No records found for the given start " +
                    timestamp + " and duratiojn "+ len + " for the camera " + sn, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("the video is being saved, please wait for a moment in order to complete the video saving progress " + sn, HttpStatus.OK);
    }


    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
    }

    private boolean isFileInScope(long created, long timestamp,long lastModify){
        if((created <= timestamp && timestamp <= lastModify) || timestamp<=created)
            return true;
        else
            return false;
    }

}
