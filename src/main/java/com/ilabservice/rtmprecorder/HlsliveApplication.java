package com.ilabservice.rtmprecorder;

import com.ilabservice.rtmprecorder.ffmpeg.FFMPEGUtils;
import com.ilabservice.rtmprecorder.util.Handler;
import com.ilabservice.rtmprecorder.util.MQTTMessageHandler;
import com.ilabservice.rtmprecorder.util.RecorderRedisHandler;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import org.influxdb.impl.InfluxDBResultMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.util.*;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableScheduling
@EnableSwagger2
public class HlsliveApplication {


	@Value("${ffmpegpath}")
	private String ffmpegpath;

	@Value("${ffprobepath}")
	private String ffprobepath;

	@Bean
	public FFmpeg ffmpeg(){

		FFmpeg ffmpeg  = null;
		try {
			ffmpeg = new FFmpeg(ffmpegpath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffmpeg;
	}

	@Bean
	FFprobe ffprobe() {
		FFprobe ffprobe = null;

		try {
			ffprobe = new FFprobe(ffprobepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ffprobe;
	}


	@Bean
	public RestTemplate restTemplate(){ return new RestTemplate();}


	@Bean
	InfluxDBResultMapper resultMapper(){
		return new InfluxDBResultMapper();
	}

	@Bean
	public FFMPEGUtils ffmpegUtils(){return  new FFMPEGUtils();}

	@Bean
	public RecorderRedisHandler recorderRedisHandler(){
		return new RecorderRedisHandler();
	}

	@Bean
	public MQTTMessageHandler mqttMessageHandler(){return new MQTTMessageHandler();}

	@Bean
	public Handler handler(){return  new Handler();}

	@Bean
	Map<String,Long> recoerderFileSizeChecking(){return new HashMap<String,Long>();}

	@Bean
	Map<String,String> recoerderFileNameChecking(){return new HashMap<String,String>();}

	public static void main(String[] args) {
		SpringApplication.run(HlsliveApplication.class, args);

	}




}
