package com.ilabservice.rtmprecorder.service.impl;

import com.ilabservice.rtmprecorder.entity.Video;
import com.ilabservice.rtmprecorder.repository.VideoRepository;
import com.ilabservice.rtmprecorder.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    VideoRepository videoRepository;

    @Override
    public List<Video> findByVideoName(String videoName) {
        return videoRepository.findByVideo(videoName);
    }

    @Override
        public void save(Video video) {
        videoRepository.save(video);
        }

    @Override
    public void save(List<Video> videoList) {
        videoRepository.saveAll(videoList);
    }

    @Override
    public Map<String,Video> findByVideoNameIn(List<String> videoNames) {
        Map<String,Video> retMap = new HashMap<String,Video>();

        List<Video> videos = videoRepository.findByVideoIn(videoNames);
        if(videos!=null){
            videos.stream().forEach(s->retMap.put(s.getVideo(),s));
        }
        return retMap;
    }

    @Override
    public List<Video> findByStartTimeAndEndTime(long startTime, long endTime) {
        List<Video> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(endTime,startTime);
        return videoList;
    }

    @Override
    public List<Video> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId) {
        List<Video> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(endTime,startTime,cameraId);
        return videoList;
    }

    @Override
    @Transactional
    public void deleteByCameraIdAndStartTime(String cameraId, long startTime) {
        videoRepository.deleteByCameraIdAndStartTime(cameraId,startTime);
    }

    @Override
    @Transactional
    public void deleteByCameraId(String cameraId) {
        videoRepository.deleteByCameraId(cameraId);
    }

    @Override
    public Video findByCameraIdAndStartTime(String cameraId, long startTime) {
        List<Video> videoList = videoRepository.findByCameraIdAndStartTime(cameraId,startTime);
        if(videoList!=null && videoList.size()>0)
            return videoList.get(0);
        else
            return null;
    }

    @Override
    public List<Video> findByCameraId(String cameraId) {
        return videoRepository.findByCameraId(cameraId);
    }

}
