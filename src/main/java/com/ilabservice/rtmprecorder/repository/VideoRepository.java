package com.ilabservice.rtmprecorder.repository;

import com.ilabservice.rtmprecorder.entity.Video;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoRepository extends JpaRepository<Video,Long> {
    List<Video> findByVideo(String video);
    List<Video> findByVideoIn(List<String> videos);
    List<Video> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(long endTime,long startTime,String cameraId);
    List<Video> findByStartTimeLessThanEqualAndEndTimeGreaterThan(long endTime,long startTime);
    void deleteByCameraIdAndStartTime(String cameraId,long startTime);
    void deleteByCameraId(String cameraId);
    List<Video> findByCameraId(String cameraId);
    List<Video> findByCameraIdAndStartTime(String cameraId, long startTime);
}
