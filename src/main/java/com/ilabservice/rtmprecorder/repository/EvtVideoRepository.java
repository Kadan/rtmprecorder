package com.ilabservice.rtmprecorder.repository;

import com.ilabservice.rtmprecorder.entity.EvtVideo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EvtVideoRepository extends JpaRepository<EvtVideo,Long>{
    List<EvtVideo> findByVideo(String video);
    List<EvtVideo> findByVideoIn(List<String> videos);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(long endTime, long startTime, String cameraId);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraIdAndVaEndTimeGreaterThanAndVaCode(long endTime, long startTime, String cameraId,Long vaEndTime,String vaCode);
    List<EvtVideo> findByStartTimeLessThanEqualAndEndTimeGreaterThan(long endTime, long startTime);
    void deleteByCameraIdAndStartTime(String cameraId,long startTime);
    void deleteByCameraId(String cameraId);
    List<EvtVideo> findByCameraId(String cameraId);
    List<EvtVideo> findByImage(String image);
    List<EvtVideo> findByCameraIdAndStartTime(String cameraId,long startTime);
    List<EvtVideo> findByCameraIdAndStartTimeAndVaresultContaining(String cameraId,long startTime,String varesult);
    List<EvtVideo> findByCameraIdAndStartTimeAndEndTimeAndVaresultContaining(String cameraId,long startTime,long endTime,String varesult);



}
