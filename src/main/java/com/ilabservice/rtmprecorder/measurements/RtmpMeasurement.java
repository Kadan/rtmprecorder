package com.ilabservice.rtmprecorder.measurements;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

import java.time.Instant;

@Measurement(name="RTMP_MEASUREMENTS")
public class RtmpMeasurement {

    public String getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = String.valueOf(time);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTotalBroadcasting() {
        return totalBroadcasting;
    }

    public void setTotalBroadcasting(int totalBroadcasting) {
        this.totalBroadcasting = totalBroadcasting;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="time")
    private String time;
    @Column(name="type")
    private String type;
    @Column(name="totalBroadcasting")
    private int totalBroadcasting;
    @Column(name="name")
    private String name;

    public int getMaxBroadcasting() {
        return maxBroadcasting;
    }

    public void setMaxBroadcasting(int maxBroadcasting) {
        this.maxBroadcasting = maxBroadcasting;
    }

    @Column(name="maxBroadcasting")
    private int maxBroadcasting;
}
