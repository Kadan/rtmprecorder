package com.ilabservice.rtmprecorder.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.rtmprecorder.entity.EvtVideo;
import com.ilabservice.rtmprecorder.service.EvtVideoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class MQTTMessageHandler {
    private static final Logger log = LoggerFactory.getLogger(MQTTMessageHandler.class);

    @Autowired
    EvtVideoService videoService;

    //- name: video.ai/v1.0/result
    //  retain: false
    //  qos: 1
    //  payload:
    //    type: object
    //    properties:
    //      code:
    //        type: integer
    //        description: 0 ok, otherwise failed
    //      msg:
    //        type: string
    //        description: OK when code is 0. otherwise is application provided error message
    //      target:
    //          type: object
    //          $ref: video.ai.api.yaml/components/schemas/task
    //      data:
    //          type: object
    //          properties:
    //            humanDetect:
    //              type: object
    //              properties:
    //                found:
    //                  type: boolean
    //                image:
    //                  type: string
    //                  example:
    //                    http://40.73.41.176/video/D72158932/1576842457000-1576842488999/firstFrame.jpg
    //{
    //   "code":0,
    //   "msg":"ok",
    //   "target":{
    //      "cameraId":"D72154040",
    //      "endTime":1577267418999,
    //      "image":"http://evcloudsvc.ilabservice.cloud/video/D72154040/1550143347000-1577267418999/firstFrame.jpg",
    //      "length":260,
    //      "startTime":1550143347000,
    //      "video":"http://evcloudsvc.ilabservice.cloud/video/D72154040/1550143347000-1577267418999/1550143347000-1577267418999.mp4"
    //   },
    //   "data":{
    //      "humanDetect":{
    //         "found":1,
    //         "level":"0.857669",
    //         "image":"detect_person_1577697851816.jpg"
    //      }
    //   }
    //}

    public void handlePayload_evt(String payload) {

        log.info(" start to save ... " + payload);
        JSONObject jsonObject = null;
        try{
            jsonObject = JSON.parseObject(payload);
        }catch(Exception e){
            log.info(" unable to parse the payload to JSON " + payload);
            return;
        }
        int vaCode = -1;
        if(jsonObject.containsKey("code")){
            vaCode = jsonObject.getIntValue("code");
            log.info("Analysis code ... " + vaCode);
            if(jsonObject.containsKey("msg"))
                log.info("Analysis result msg ... " + jsonObject.containsKey("msg"));
        }else{
            log.info("Invalid response no vaCode returned ... ");
//            return;
        }

        String cameraId = "";
        Long startTime = 0L;
        String image = "";

        if(jsonObject.containsKey("target")){
            JSONObject target = jsonObject.getJSONObject("target");
            if(target.containsKey("cameraId"))
                cameraId = target.getString("cameraId");
            else{
                log.info("Analysis result does not contains any camera ID in target ... ");
                //return;
            }
            if(target.containsKey("startTime"))
                startTime = target.getLong("startTime");
            else{
                log.info("Analysis result does not contains any startTime in target ... ");
                //return;
            }
            if(target.containsKey("image"))
                image = target.getString("image");
            else{
                log.info("Analysis result does not contains any startTime in target ... ");
                //return;
            }
        }else{
            log.info("Analysis result does not contains any target ... ");
            return;
        }

        EvtVideo video = null;
        if(startTime > 0 && StringUtils.isNotEmpty(cameraId))
            video = videoService.findByCameraIdAndStartTime(cameraId,startTime);
        else if(StringUtils.isNotEmpty(image))
            video = videoService.findByImage(image);

        if (video == null) {
            log.info("No suitable video found ");
            return;
        }

        if(jsonObject.containsKey("data")) {
            JSONObject data = jsonObject.getJSONObject("data");
            log.info("start to update repository with data " + JSON.toJSONString(data));
            video.setVaresult(JSON.toJSONString(data));
            video.setVaCode(String.valueOf(vaCode)); // 0 is okay, otherwise failure ....
            video.setVaEndTime(System.currentTimeMillis());
            videoService.save(video);
        }else{
            log.info("save the exceptional case ");
//            video.setVaresult(JSON.toJSONString(data));
            video.setVaCode(String.valueOf(vaCode)); // 0 is okay, otherwise failure ....
            video.setVaEndTime(System.currentTimeMillis());
            videoService.save(video);

//            return;
        }
    }
}
