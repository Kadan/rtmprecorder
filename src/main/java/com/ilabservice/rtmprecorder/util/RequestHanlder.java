package com.ilabservice.rtmprecorder.util;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

public class RequestHanlder {

    public String getRequestJson(HttpServletRequest request){
        BufferedReader bfr = null;
        StringBuilder sb = new StringBuilder("");
        try {
            bfr = request.getReader();
            String str;
            while ((str = bfr.readLine()) != null) {
                sb.append(str);
            }
            bfr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bfr) {
                try {
                    bfr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }
}
